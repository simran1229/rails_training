class CreateLibraries < ActiveRecord::Migration[5.0]
  def change
    create_table :libraries do |t|
       t.string  :name
       t.string  :author
       t.string  :place
       t.integer :no
       t.timestamps
    end
  end
end
