class AddColumnToInfo < ActiveRecord::Migration[5.0]
  def change
  	add_column :infos , :name    , :string
  	add_column :infos , :age     , :integer
  	add_column :infos , :password, :string
  	add_column :infos , :address , :string
  end
end
