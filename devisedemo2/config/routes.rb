Rails.application.routes.draw do
  devise_for :users
  root to: 'user_login#login_page'
  get '/login',    to: 'user_login#login_page'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
