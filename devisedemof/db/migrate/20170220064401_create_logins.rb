class CreateLogins < ActiveRecord::Migration[5.0]
  def change
    create_table :logins do |t|
      t.string   :name
      t.string   :address
      t.integer  :age
      t.timestamps
    end
  end
end
