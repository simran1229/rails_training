class Friend < ApplicationRecord
    has_one  :family , through: :personal
    has_and_belongs_to_many :personal
end
