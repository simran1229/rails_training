class CreateFamilies < ActiveRecord::Migration[5.0]
  def change
    create_table :families do |t|
       t.string  :name
       t.string  :phn_no
       t.string  :relation
       t.timestamps
    end
  end
end
