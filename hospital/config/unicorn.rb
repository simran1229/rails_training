# Set the current app's path for later reference. Rails.root isn't available at
# this point, so we have to point up a directory.
app_path = '/home/beryl/Desktop/rails_training/hospital'
# The number of worker processes you have here should equal the number of CPU
# cores your server has.
worker_processes (ENV['RAILS_ENV'] == 'production' ? 4 : 1)


timeout 300
# Set the working directory of this unicorn instance.
working_directory app_path

pid app_path + '/tmp/unicorn.pid'

stderr_path app_path + '/log/unicorn.log'
stdout_path app_path + '/log/unicorn.log'
