class CreateDoctors < ActiveRecord::Migration[5.0]
   def change
     create_table :doctors do |t|
        t.string  :name
        t.integer :age
        t.integer :salary
        t.string  :address
        t.timestamps
     end
   end
end
