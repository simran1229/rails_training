Rails.application.routes.draw do
  get 'login' => 'sessions#new', :as => 'login'
  get 'logout' => 'sessions#destroy', :as => 'logout'
  
   
# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
