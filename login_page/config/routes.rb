Rails.application.routes.draw do
  root 'welcome_page#first_page'

  get '/new',    to: 'welcome_page#new_page'
  get '/old',    to: 'welcome_page#old_page'
  get '/details',to: 'welcome_page#detail_page'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
