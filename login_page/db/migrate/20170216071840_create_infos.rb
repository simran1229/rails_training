class CreateInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :infos do |t|
       t.string  :name
       t.integer :age
       t.string  :password
       t.string  :address
       t.timestamps
    end
  end
end
