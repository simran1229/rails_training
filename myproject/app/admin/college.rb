ActiveAdmin.register College do
permit_params(:name, :university, :address, :logo, :ug_courses, :pg_courses,:email , :password, :phone_no, :students, :campus_size)
  before_filter do 
    @college = College.friendly.find(params[:id]) if params[:id].present?
  end
 # Create sections on the index screen
  scope :all, default: true

  # Filterable attributes on the index screen
  filter :name
  filter :description

  # Customize columns displayed on the index screen in the table
  index do
    column "id"
    column "name"
    column "university"
    column "address"
    column "logo"
    column "phone_no"
    actions
  end
  form do |f|
    f.inputs "Create Center" do
      f.input :name
      f.input :university
      f.input :address
      f.input :ug_courses , :as => :text
      f.input :pg_courses , :as => :text
      f.input :phone_no
      f.input :email
      f.input :password
      f.input :logo , :type => :file
      f.input :students
      f.input :campus_size
    end
    f.actions
  end

  before_save do |college|
    college.ug_courses = params[:college][:ug_courses].split("\n") unless params[:college].nil? or params[:college][:ug_courses].nil?
    college.pg_courses = params[:college][:pg_courses].split("\n") unless params[:college].nil? or params[:college][:pg_courses].nil?
  end
end
