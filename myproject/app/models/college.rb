class College < ActiveRecord::Base
  mount_uploader :logo, LogoUploader
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  extend FriendlyId
	friendly_id :name, use: :slugged
	# serialize :pg_courses, Array
	# serialize :ug_courses, Array
end
