Rails.application.routes.draw do
 
  devise_for :colleges
  mount Refinery::Core::Engine, at: Refinery::Core.mounted_path

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root 'home#index'
  get '/college/:college_slug' => 'colleges#details'
  post '/collge/:college_slug' => 'colleges#details'
end
