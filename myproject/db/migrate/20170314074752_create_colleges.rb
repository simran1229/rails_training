class CreateColleges < ActiveRecord::Migration
  def change
    create_table :colleges do |t|
      t.string   :name 
      t.string   :university
      t.string   :address
      t.string   :logo
      t.string   :pg_courses , array: true
      t.string   :ug_courses , array: true
      t.string   :phone_no
      t.string   :students
      t.string   :campus_size
      t.timestamps null: false
    end
  end
end
