ActiveAdmin.register Employee do
 permit_params(:first_name, :last_name, :address, :phone_no,  :images, :email , :password, :joining_date, :salary)

  before_filter :only => [:show, :edit] do
  end

  # Create sections on the index screen
  scope :all, default: true

  # Filterable attributes on the index screen
  filter :first_name
  filter :images
  filter :address
  filter :phone_no
  filter :email
  filter :password
  # Customize columns displayed on the index screen in the table
  index do
    column "id"
    column "first_name"
    column "last_name"
    column "address"
    column "phone_no"
    column "images"
    column "joining_date"
    column "email"
    column "password"
    column "salary"
    actions
  end
  form do |f|
    f.inputs "Create Center" do
      f.input :first_name
      f.input :last_name
      f.input :address
      f.input :phone_no
      f.input :joining_date
      f.input :salary
      f.input :email
      f.input :password
    end
    f.actions
  end

end
