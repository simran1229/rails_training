class Employee < ActiveRecord::Base
    mount_uploader :images, ImagesUploader 
    devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
   
end
