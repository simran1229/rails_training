class CreateEmployees < ActiveRecord::Migration[5.0]
  def change
    create_table :employees do |t|
       t.string  :name     
       t.integer :salary   
       t.string  :address
       t.string  :phone_no , :limit => 10 
       t.timestamps
    end
  end
end
