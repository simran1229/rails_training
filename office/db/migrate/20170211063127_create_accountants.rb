class CreateAccountants < ActiveRecord::Migration[5.0]
  def change
    create_table :accountants do |t|
      t.references :employee, foreign_key: true
      t.string :name

      t.timestamps
    end
  end
end
