class AddIndexToAccountants < ActiveRecord::Migration[5.0]
  def change
    add_column :accountants, :dep_no, :string
    add_index :accountants, :dep_no
  end
end
