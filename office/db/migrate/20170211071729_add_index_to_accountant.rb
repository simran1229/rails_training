class AddIndexToAccountant < ActiveRecord::Migration[5.0]
  def change
     add_index :accountants, :name
  end
end
