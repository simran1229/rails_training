class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
       t.string  :first_name
       t.string  :last_name
       t.string  :images
       t.string  :address
       t.string  :phone_no
       t.string  :joining_date
       t.string  :salary
       t.timestamps null: false
    end
  end
end
