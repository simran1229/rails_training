class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
       t.string  :name
       t.string  :cost
       t.string  :duration
       t.integer :employees_no
       t.belongs_to :employees
       t.timestamps null: false
    end
  end
end
