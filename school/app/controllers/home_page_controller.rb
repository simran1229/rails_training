class HomePageController < ApplicationController
  def firstpage
   @student_name = Student.select(:c_name)
   @teacher_name = Teacher.all
   @library_in   = Library.all
   @max_per      = Student.select(:c_name, :percentage).where('percentage IN (SELECT MAX(percentage) from students)')
   @min_per      = Student.select(:c_name, :percentage).where('percentage IN (SELECT MIN(percentage) from students)')
   @book_teach   = Teacher.select("teachers.name ,libraries.b_name").joins("INNER JOIN libraries on libraries.id = teachers.libraries_id")
   @book_stud    = Student.select("students.c_name ,libraries.b_name").joins("INNER JOIN libraries on libraries.id = students.libraries_id")
   @aut_stud     = Library.joins("INNER JOIN teachers ON libraries.id = teachers.libraries_id").joins("INNER JOIN students ON libraries.id = students.libraries_id").select(:b_author , :name,:c_name)


  end

end
