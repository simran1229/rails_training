class CreateLibraries < ActiveRecord::Migration[5.0]
  def change
    create_table :libraries do |t|
      t.string   :b_name
      t.string   :b_author
      t.timestamps
    end
  end
end
