class CreateStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :students do |t|
     t.string  :c_name
     t.string :c_class
     t.integer :percentage
      t.timestamps
    end
  end
end
