class CreateAddLibraryToTeachers < ActiveRecord::Migration[5.0]
  def change
    create_table :add_library_to_teachers do |t|
      add_reference :teachers, :libraries, foreign_key: true

      t.timestamps
    end
  end
end
