class CreateAddLibrariesToStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :add_libraries_to_students do |t|
      add_reference :students, :libraries, foreign_key: true

      t.timestamps
    end
  end
end
