class CreateAddTeacherToStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :add_teacher_to_students do |t|
      add_reference :students, :teachers, foreign_key: true
      t.timestamps
    end
  end
end
