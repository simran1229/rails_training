# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170210073217) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "add_libraries_to_students", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "add_library_to_students", force: :cascade do |t|
    t.integer  "students_id"
    t.integer  "libraries_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["libraries_id"], name: "index_add_library_to_students_on_libraries_id", using: :btree
    t.index ["students_id"], name: "index_add_library_to_students_on_students_id", using: :btree
  end

  create_table "add_library_to_teachers", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "add_teacher_to_students", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "libraries", force: :cascade do |t|
    t.string   "b_name"
    t.string   "b_author"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "students", force: :cascade do |t|
    t.string   "c_name"
    t.string   "c_class"
    t.integer  "percentage"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "libraries_id"
    t.integer  "teachers_id"
    t.index ["libraries_id"], name: "index_students_on_libraries_id", using: :btree
    t.index ["teachers_id"], name: "index_students_on_teachers_id", using: :btree
  end

  create_table "teachers", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.integer  "class_no"
    t.string   "subject"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "libraries_id"
    t.index ["libraries_id"], name: "index_teachers_on_libraries_id", using: :btree
  end

  add_foreign_key "add_library_to_students", "libraries", column: "libraries_id"
  add_foreign_key "add_library_to_students", "students", column: "students_id"
  add_foreign_key "students", "libraries", column: "libraries_id"
  add_foreign_key "students", "teachers", column: "teachers_id"
  add_foreign_key "teachers", "libraries", column: "libraries_id"
end
